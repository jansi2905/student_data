"""student_ranking URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url,include
from teacher import views as teacher_views
from django.contrib.auth import views as auth_views
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required


urlpatterns = [
    url('admin/', admin.site.urls),
    url('accounts/', include('django.contrib.auth.urls')),

        url(
        r'^$',
        teacher_views.LoginSuccess.as_view(),
        name='landing_page'
    ),
    url(r'^accounts/', include(
        'django_registration.backends.one_step.urls')),
    url(
        r'^accounts/login/$',
        auth_views.login, {
            'template_name': 'django_registration/login.html'
        },
        name='login'
    ),

    url(r'^accounts/logout/$', auth_views.logout, {'next_page': '/accounts/login'}, name='logout'),




]
