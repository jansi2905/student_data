from __future__ import division
from django.shortcuts import render, render_to_response
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.shortcuts import redirect, get_object_or_404, HttpResponse
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum
from teacher.models import *


# Create your views here.
class LoginSuccess(LoginRequiredMixin, TemplateView):
    template_name = "marks_form.html"

    def get_context_data(self, **kwargs):
        context = super(LoginSuccess, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:

            if(self.request.user.is_admin or self.request.user.is_superuser):
                    
                context['students'] = Students.objects.all()
                context['subjects'] = Subject.objects.all() 
                context['all_marks'] = Marks.objects.all()
            else:
                context['students'] = Students.objects.all()
            
                context["subject"] = self.request.user.subject
                context["subject_id"] = self.request.user.subject.id
                context['marks'] = Marks.objects.filter(subject=self.request.user.subject)    
        return context
    
    
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            self.template_name = "marks_form.html"
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


    def post(self, request, *args, **kwargs):
        data = dict(request.POST)
        data.pop('csrfmiddlewaretoken')
        data.pop('total_marks_length')
           
        for key,value in data.items():
            marks,student_id = key.split('_')
            if not Marks.objects.filter(student_id=student_id,subject = self.request.user.subject).exists():
                student_mark = Marks.objects.create(
                    student_id=student_id,subject = self.request.user.subject, marks= value[0])
            else:
                student_mark = Marks.objects.filter(student_id=student_id,subject = self.request.user.subject)[0]
            student_mark.marks = value[0]
            student_mark.save()

            #Average
            a = Marks.objects.filter(student_id=student_id  ).aggregate(sum = Sum('marks'))
            all_subject_marks = a['sum']
            total_subject_count = Subject.objects.all().count()
            average = all_subject_marks / total_subject_count
            student = Students.objects.get(id = student_id)
            student.average = average
            student.save()


            #ranking
            rank = 0
            dicts= {}
            averarge_list = Students.objects.values_list('average')
            averaging = sorted(averarge_list, reverse=True)
            skip = 0
            prev = None

            for average in averaging:
                if average == prev:
                    skip+=1
                else:
                    rank+= skip+1 
                    skip=0
                dicts.update({rank:average})
                prev = average
            print(dicts,"dsffds")           
                 
            for key,value in dicts.items():
                for student in Students.objects.all():
                    if float(value[0]) == student.average:
                        student.rank = key
                        student.id = student.id
                        student.save()
                        print(student.rank,student)
              
        return redirect(reverse('landing_page'))    