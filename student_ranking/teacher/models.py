from django.db import models

# Create your models here.

from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils import timezone
from django.urls import reverse_lazy, reverse
from django.core.exceptions import ObjectDoesNotExist


class Subject(models.Model):
    name = models.CharField(max_length=250,
        null=True, blank=True)
    code = models.IntegerField(unique=True)

    def __str__(self):
        '''To represent object'''
        return '{}-{}'.format(self.name, self.code)



class Teacher(AbstractUser):
    # extending following to the django built user model
    subject = models.OneToOneField(
        Subject,null=True)
    is_admin = models.BooleanField(
        default=False)

    def __str__(self):
        '''To represent object'''
        return '{}-{}'.format(self.username, self.subject)       

class Students(models.Model):
    name = models.CharField(max_length=250,
        null=True,blank=True)
    rank = models.IntegerField(null=True)
    average = models.FloatField(null=True)

    def __str__(self):
        '''To represent object'''
        return '{}'.format(self.name)

    def get_marks(self):
        marks = Marks.objects.get(student_id=self.id)   
        return marks

class Marks(models.Model):
    subject = models.ForeignKey(Subject,
        on_delete=models.CASCADE,
        null=True)
    marks = models.IntegerField()
    student = models.ForeignKey(Students,
        on_delete=models.CASCADE,
        null=True)
    def __str__(self):
        '''To represent object'''
        return '{}-{}'.format(self.student,self.subject)

