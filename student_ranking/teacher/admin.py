from django.contrib import admin


# Register your models here.
from django.utils.translation import ugettext_lazy as _
from teacher.models import Teacher,Subject,Students,Marks
from django.contrib.auth.admin import UserAdmin

# class TeacherAdmin(UserAdmin):
#     pass


UserAdmin.fieldsets = (
    (None, {'fields': ('username', 'password')}),
    (_('Subject info'), {'fields': ('subject','is_admin')}),
    (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser')}),
)

admin.site.register(Teacher, UserAdmin)
admin.site.register(Subject)
admin.site.register(Students)
admin.site.register(Marks)
