from django import template
from django.conf import settings
from teacher.models import *
register = template.Library()


@register.filter(name='get_marks')
def get_marks(student_id, subject_id):
    if Marks.objects.filter(student_id=student_id, subject_id=subject_id).exists():
        subject_mark = Marks.objects.filter(student_id=student_id, subject_id=subject_id)[0]
        marks = subject_mark.marks
    else:
        marks = None    
    return marks