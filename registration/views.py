"""
A one-step (user signs up and is immediately active and logged in)
workflow.

"""

from django.contrib.auth import authenticate, get_user_model, login
from django.urls import reverse_lazy

from django_registration import signals
from django_registration.views import RegistrationView as BaseRegistrationView


User = get_user_model()


class RegistrationView(BaseRegistrationView):
    """
    register new user without auto login

    """
    success_url = reverse_lazy('django_registration_complete')

    def register(self, form):
        new_user = form.save()
        signals.user_registered.send(
            sender=self.__class__,
            user=new_user,
            request=self.request
        )
        return new_user