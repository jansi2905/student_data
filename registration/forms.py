from django_registration.forms import RegistrationForm
from django.contrib.auth import get_user_model

from employee.models import User


class EmployeeUserForm(RegistrationForm):
    class Meta(RegistrationForm.Meta):
        model = User
        fields = [
            User.USERNAME_FIELD,
            User.get_email_field_name(),
            'first_name',
            'last_name',
            'password1',
            'password2',
            'employee_id',
            'company',
            'level',
            'report_to'
        ]